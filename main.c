// System Includes
#include <stdio.h>
#include <stdbool.h>

// SDL Includes
#include "SDL.h"
#include "SDL_image.h"

// LUA Includes
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

// Project Includes
#include "RyanLogger.h"
#include "messages.h"

static int msghandler (lua_State *L) {
  const char *msg = lua_tostring(L, 1);
  if (msg == NULL) {  /* is error object not a string? */
    if (luaL_callmeta(L, 1, "__tostring") &&  /* does it have a metamethod */
        lua_type(L, -1) == LUA_TSTRING)  /* that produces a string? */
      return 1;  /* that is the message */
    else
      msg = lua_pushfstring(L, "(error object is a %s value)",
                               luaL_typename(L, 1));
  }
  luaL_traceback(L, L, msg, 1);  /* append a standard traceback */
  return 1;  /* return the traceback */
}


/*
** Interface to 'lua_pcall', which sets appropriate message function
** and C-signal handler. Used to run all chunks.
*/
static int docall (lua_State *L, int narg, int nres) {
  int status;
  int base = lua_gettop(L) - narg;  /* function index */
  lua_pushcfunction(L, msghandler);  /* push message handler */
  lua_insert(L, base);  /* put it under function and args */
  status = lua_pcall(L, narg, nres, base);
  lua_remove(L, base);  /* remove message handler from the stack */
  return status;
}


int main(int argc, char* argv[])
{  
   SDL_Window * window; 
   SDL_Renderer * rend;
   SDL_Event event;
   
   
   Uint32 prevTicks;
   bool running;
   int luaResult;
   const char * windowTitle = "ORPE2D";
   
   lua_State * L = luaL_newstate();
   luaL_openlibs(L);
   luaResult = luaL_loadfile(L, "config.lua");
   if(luaResult == 0)
   {
      luaResult = docall(L, 0, 0);
      if(luaResult != 0)
      {
         rlmLogError("luaL_dofile result: %i", luaResult);
         rlmLogError("Lua Message: %s", lua_tostring(L, -1));
      }
      else
      {
         lua_getglobal(L, "GameName");
         windowTitle = luaL_checkstring(L, -1);
         lua_pop(L, 1);
      }
   }
   

   SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2
   IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF);

   // Create an application window with the following settings:
   window = SDL_CreateWindow(
      windowTitle,                      // window title
      SDL_WINDOWPOS_UNDEFINED,           // initial x position
      SDL_WINDOWPOS_UNDEFINED,           // initial y position
      1024,                       // width, in pixels
      768,                      // height, in pixels
      SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
   
   // Check that the window was successfully created
   if (window == NULL) {
      // In the case that the window could not be made...
      printf("Could not create window: %s\n", SDL_GetError());
      return 1;
   }
   
   rend = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
   
   
   running = true;
   prevTicks = SDL_GetTicks();
   while(running)
   {
      Uint32 ticks;
      while(SDL_PollEvent(&event))
      {
         if(event.type == SDL_QUIT)
         {
            running = false;
         }
         else if(event.type == SDL_WINDOWEVENT)
         {
            if(event.window.event == SDL_WINDOWEVENT_RESIZED)
            {
               //ScreenWidth  = event.window.data1;
               //ScreenHeight = event.window.data2;
            }
         }
      }

      ticks = SDL_GetTicks();
      //update((double)(ticks - prevTicks) / 1000.0);
      prevTicks = ticks;

      //render(rend);
      SDL_SetRenderDrawColor(rend, 0x00, 0x00, 0x40, SDL_ALPHA_OPAQUE);
      SDL_RenderClear(rend);
      
      
      
      // Show to user
      SDL_RenderPresent(rend);
      
   }
   
   lua_close(L);
   // Close and destroy the window
   SDL_DestroyWindow(window);
  
   // Clean up
   IMG_Quit();
   SDL_Quit();
   rlmLogDebug("End of program");
   return 0;
}

