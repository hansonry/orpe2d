cmake_minimum_required(VERSION 2.8.9)

project(ORPE2D)

# Uncomment following line to enable debug builds
#set(CMAKE_BUILD_TYPE Debug)

if(WIN32)
   set(SDL2_PATH       "${CMAKE_CURRENT_LIST_DIR}/../SDL2-2.0.14/x86_64-w64-mingw32")
   set(SDL2_IMAGE_PATH "${CMAKE_CURRENT_LIST_DIR}/../SDL2_image-2.0.5/x86_64-w64-mingw32")
   #set(SDL2_TTF_PATH   "${CMAKE_CURRENT_LIST_DIR}/../SDL2_ttf-2.0.15/x86_64-w64-mingw32")

   set(SDL2_LIBRARIES         "mingw32;${SDL2_PATH}/lib/libSDL2main.a;${SDL2_PATH}/lib/libSDL2.dll.a")
   set(SDL2_INCLUDE_DIRS      "${SDL2_PATH}/include/SDL2")
   
   set(SDL2_IMAGE_LIBRARY     "${SDL2_IMAGE_PATH}/lib/libSDL2_image.dll.a")
   set(SDL2_IMAGE_INCLUDE_DIR "${SDL2_IMAGE_PATH}/include/SDL2")
   
   #set(SDL2_TTF_LIBRARY       "${SDL2_TTF_PATH}/lib/libSDL2_ttf.dll.a")
   #set(SDL2_TTF_INCLUDE_DIR   "${SDL2_TTF_PATH}/include/SDL2")

   set(LUA_LIBRARY "lua")   
   set(LUA_INCLUDE_DIR "")
  
else()   
   set(SDL2_LIBRARIES "SDL2")
   set(SDL2_INCLUDE_DIRS  "/usr/include/SDL2")
   
   set(SDL2_IMAGE_LIBRARY "SDL2_image")
   set(SDL2_IMAGE_INCLUDE_DIR  "")
   
   #set(SDL2_MIXER_LIBRARIES "SDL2_mixer")
   #set(SDL2_MIXER_INCLUDES  "")
   set(LUA_LIBRARY "lua5.3")
   set(LUA_INCLUDE_DIR "/usr/include/lua5.3")

endif()



include_directories(${SDL2_INCLUDE_DIRS} ${SDL2_IMAGE_INCLUDE_DIR} ${LUA_INCLUDE_DIR})
add_executable(orpe2d
   main.c
   List.c
   RyanLogger.c
)
target_link_libraries(orpe2d ${SDL2_LIBRARIES} ${SDL2_IMAGE_LIBRARY} ${LUA_LIBRARY})

