// This code is automaticaly generated
// Do not edit by hand
#ifndef __MESSAGES_H__
#define __MESSAGES_H__
#include <stdint.h>
#include "RyanLogger.h"

// Message IDs
#define MSG_ID_VERSION 1

// Message Structures
//// Version
struct msgVersion
{
   uint16_t messageID;
   int16_t major;
   int16_t minor;
};
//// msg
union msg
{
   uint16_t messageID;
   struct msgVersion msgVersion;
};

// Logging Message
//// Version
static inline
void msgVersionLogDebug(const struct msgVersion * msgVersion)
{
   rlmLogDebug("Message Version (1):");
   rlmLogDebug("   major: %u", msgVersion->major);
   rlmLogDebug("   minor: %u", msgVersion->minor);
}

static inline
void msgLogDebug(const union msg * msg)
{
   switch(msg->messageID)
   {
   case MSG_ID_VERSION:
      msgVersionLogDebug(&msg->msgVersion);
      break;
   default:
      rlmLogError("Unknown messageID: %d", msg->messageID);
      break;
   }
}

#endif // __MESSAGES_H__

