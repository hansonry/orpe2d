-- This file defines messages that will be used to communicate between the
-- server and the client.


local messages = {}

function addMessage(name, id, params)
   assert(type(name) == "string")
   assert(type(id) == "number")
   assert(type(params) == "table")
   local msg = { name = name, id = math.floor(id), params = params}
   table.insert(messages, msg)
   return msg
end


function makeSignedIntParam(name, sizeInBytes)
   return {type = "SInt", name = name, sizeInBytes=sizeInBytes}
end

function makeUnsignedIntParam(name, sizeInBytes)
   return {type = "UInt", name = name, sizeInBytes = sizeInBytes}
end

function makeFloat32Param(name)
   return {type = "Float32", name = name}
end



addMessage("Version", 1, {
   makeUnsignedIntParam("major", 2),
   makeUnsignedIntParam("minor", 2),
})




return { messages = messages }


