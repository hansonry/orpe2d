

local codeGenC = {}

function nameToDefine(filename)
   return filename:upper():gsub("%.", "_")
end

function nameToMessageIDConst(name)
   return string.format("MSG_ID_%s", name:upper())
end

function makeStructName(structPrefix, name)
   return string.format("%s%s", structPrefix, name)
end

function codeGenC.generate(defs, sourceFileName)
   local fileHandle = io.open(sourceFileName, "w")

   local sf = string.format

   local structPrefix = "msg"
   
   function fwrite(format, ...)
      fileHandle:write(sf(format, ...))
   end


   local blockerName = sf("__%s__", nameToDefine(sourceFileName))

   fwrite("// This code is automaticaly generated\n")
   fwrite("// Do not edit by hand\n")
   fwrite("#ifndef %s\n", blockerName)
   fwrite("#define %s\n", blockerName)
  
   fwrite("#include <stdint.h>\n")
   fwrite("#include \"RyanLogger.h\"\n")
 
   fwrite("\n")

   local messages = defs.messages

   fwrite("// Message IDs\n")
   for i,v in ipairs(messages) do
      local idConst = nameToMessageIDConst(v.name)
      fwrite("#define %s %d\n", idConst, v.id)
   end
   fwrite("\n")

   local messageIDName = "messageID"
   local messageIdElement = sf("uint16_t %s;", messageIDName)

   fwrite("// Message Structures\n")
   
   function writeStructParams(message)
      local typeActions = {}
      function typeActions.SInt(param)
         fwrite("   int%d_t %s;\n", param.sizeInBytes * 8, param.name)
      end
      function typeActions.UInt(param)
         fwrite("   int%d_t %s;\n", param.sizeInBytes * 8, param.name)
      end
      function typeActions.Float32(param)
         fwrite("   float %s;\n", param.name)
      end
      for i, v in ipairs(message.params) do
         typeActions[v.type](v)
      end
   end
   for i,v in ipairs(messages) do
      fwrite("//// %s\n", v.name)
      fwrite("struct %s\n", makeStructName(structPrefix, v.name))
      fwrite("{\n")
      fwrite("   %s\n", messageIdElement)
      writeStructParams(v)
      fwrite("};\n")
   end
   fwrite("//// msg\n")
   fwrite("union %s\n", structPrefix)
   fwrite("{\n")
   fwrite("   %s\n", messageIdElement)
   for i,v in ipairs(messages) do
      local structName = makeStructName(structPrefix, v.name)
      fwrite("   struct %s %s;\n", structName, structName)
   end
   fwrite("};\n")
   
   fwrite("\n")

   fwrite("// Logging Message\n")
   function writeLogingParam(message, structName)
      local typeActions = {}
      function typeActions.UInt(param)
         fwrite("   rlmLogDebug(\"   %s: %%u\", %s->%s);\n", param.name, structName, param.name)
      end
      function typeActions.SInt(param)
         fwrite("   rlmLogDebug(\"   %s: %%d\", %s->%s);\n", param.name, structName, param.name)
      end
      function typeActions.Float32(param)
         fwrite("   rlmLogDebug(\"   %s: %%f\", %s->%s);\n", param.name, structName, param.name)
      end
      for i, v in ipairs(message.params) do
         typeActions[v.type](v)
      end
   end
   for i,v in ipairs(messages) do
      fwrite("//// %s\n", v.name)
      local structName = makeStructName(structPrefix, v.name)
      fwrite("static inline\nvoid %sLogDebug(const struct %s * %s)\n", structName, structName, structName)
      fwrite("{\n")
      fwrite("   rlmLogDebug(\"Message %s (%d):\");\n", v.name, v.id)
      writeLogingParam(v, structName)
      fwrite("}\n")
      fwrite("\n")
   end
   fwrite("static inline\nvoid %sLogDebug(const union %s * %s)\n", structPrefix, structPrefix, structPrefix)
   fwrite("{\n")
   local msgID = sf("%s->%s", structPrefix, messageIDName)
   fwrite("   switch(%s)\n", msgID)
   fwrite("   {\n")
   
   for i,v in ipairs(messages) do
      local idConst = nameToMessageIDConst(v.name)
      local structName = makeStructName(structPrefix, v.name)
      fwrite("   case %s:\n", idConst)
      fwrite("      %sLogDebug(&%s->%s);\n", structName, structPrefix, structName)
      fwrite("      break;\n")
   end
   fwrite("   default:\n")
   fwrite("      rlmLogError(\"Unknown %s: %%d\", %s);\n", messageIDName, msgID)
   fwrite("      break;\n")
   fwrite("   }\n")
   fwrite("}\n")
   fwrite("\n")


   fwrite("#endif // %s\n\n", blockerName)


   fileHandle:close()
end


return codeGenC

